var temperature = [21.0,21.0,21.0,21.0,21.0,21.0,21.0,21.0,21.0,21.0,21.0];
	for (i = 0; i< 10; i++)
	{
	var idTemp = "temp"+(i+1);
	document.getElementById(idTemp).innerHTML = temperature[i].toFixed(1) + "&#8451";
	}
	
	function addTemp(tempId)
	{
	var idTemp = "temp"+tempId;
	temperature[tempId-1] += 0.5;
	document.getElementById(idTemp).innerHTML = temperature[tempId-1].toFixed(1) + "&#8451";
	}
	
	function minusTemp(tempId)
	{
	var idTemp = "temp"+tempId;
	temperature[tempId-1] -= 0.5;
	document.getElementById(idTemp).innerHTML = temperature[tempId-1].toFixed(1) + "&#8451";
	}
	
	function timeRefresh()
	{
	var date = new Date();
	var days = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
	var day = days[date.getDay()];
	var dayOfMonth = date.getDate();
	var month = date.getMonth()+1;
	var year = date.getFullYear();
	var hour = date.getHours();
	if (hour < 10)
	{
	hour = "0"+hour;
	}
	var minute = date.getMinutes();
	if (minute < 10)
	{
	minute = "0"+minute;
	}
	var second = date.getSeconds();
	if (second < 10)
	{
	second = "0"+second;
	}
	document.getElementById("todayDate").innerHTML = day + " " + dayOfMonth + "/" + month + "/" + year;
	document.getElementById("actualTime").innerHTML = hour + ":" + minute +":" + second;
	setTimeout(timeRefresh,1000);
	}
	
	function turnOn(buttonId)
	{
	var idOn = "buttonOn"+buttonId;
	var idOff = "buttonOff"+buttonId;
	document.getElementById(idOn).style.backgroundColor = "green";
	document.getElementById(idOff).style.backgroundColor = "rgb(30,30,30)";
	}
	function turnOff(buttonId)
	{
	var idOn = "buttonOn"+buttonId;
	var idOff = "buttonOff"+buttonId;
	document.getElementById(idOn).style.backgroundColor = "rgb(30,30,30)";
	document.getElementById(idOff).style.backgroundColor = "rgb(200,0,0)";
	}